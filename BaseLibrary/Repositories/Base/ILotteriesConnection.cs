using BaseLibrary.Repositories.Base.Entities;

namespace BaseLibrary.Repositories.Base
{
    public interface ILotteriesConnection
    {
        T PostData<T>(string endpointUrl, IPostObject postData) where T : new();
    }
}
