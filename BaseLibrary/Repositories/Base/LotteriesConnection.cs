using BaseLibrary.Repositories.Base.Entities;
using RestSharp;
using System.Net;
using System.Net.Http;
using System.Text;

namespace BaseLibrary.Repositories.Base
{
    public class LotteriesConnection : ILotteriesConnection
    {
        private readonly RestClient _restClient;

        public LotteriesConnection(string endpointUrl)
        {
            _restClient = new RestClient(endpointUrl)
            {
                Encoding = new UTF8Encoding()
            };
        }

        public T PostData<T>(string endpointUrl, IPostObject postData) where T : new()
        {
            var response = PostDataCheckForStatusCodes<T>(endpointUrl, postData);
            return response;
        }

        private T PostDataCheckForStatusCodes<T>(string endpointUrl, IPostObject postData) where T : new()
        {
            var request = new RestRequest(endpointUrl, Method.POST);
            request.RequestFormat = DataFormat.Json;
            request.AddJsonBody(postData);

            var response = _restClient.Post<T>(request);

            if (response.IsSuccessful)
                return response.Data;

            throw new HttpRequestException(BuildUpExceptionMessage(endpointUrl, response.StatusCode, response.Content), response.ErrorException);
        }

        private string BuildUpExceptionMessage(string endpointUrl, HttpStatusCode statusCode, string contentType)
        {
            StringBuilder errorMessage = new StringBuilder($"error connecting to {endpointUrl} status code: {statusCode} content type: {contentType} parameters:");

            return errorMessage.ToString();
        }
    }
}
