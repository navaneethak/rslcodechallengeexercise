namespace BaseLibrary.Repositories.Base.Entities
{
    /// <summary>
    /// Only objects with this interface type can be posted 
    /// </summary>
    public interface IPostObject
    {
    }
}
