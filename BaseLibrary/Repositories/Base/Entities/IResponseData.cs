namespace BaseLibrary.Repositories.Base.Entities
{
    public interface IResponseData : IResponseObject
    {
        int Id { get; set; }
    }
}
