﻿using RSLCodeChallengeExercise.Models;
using RSLCodeChallengeExercise.Repositories.Interfaces;
using System.Collections.Generic;
using System.Web.Mvc;

namespace RSLCodeChallengeExercise.Controllers
{
    public class LottoController : Controller
    {
        readonly ILottoRepository _lottoRepository;
        public LottoController(ILottoRepository lottoRepository)
        {
            _lottoRepository = lottoRepository;
        }
        public ActionResult OpenDraws()
        {
            OpenDrawsRequest openDrawsRequest = new OpenDrawsRequest
            {
                CompanyId = LotteriesCompany.GoldenCasket,
                MaxDrawCount = 2
            };
            List<LotteriesProduct> optionalProductFilter = new List<LotteriesProduct>
            {
                LotteriesProduct.OzLotto,
                LotteriesProduct.Powerball
            };
            openDrawsRequest.OptionalProductFilter=optionalProductFilter;
            OpenDrawsResponse openDrawsResponse= _lottoRepository.GetOpenLotteriesDraw(openDrawsRequest);
            return View(openDrawsResponse);
        }
    }
}