﻿using BaseLibrary.Repositories.Base.Entities;
using System.Collections.Generic;

namespace RSLCodeChallengeExercise.Models
{
    public class OpenDrawsRequest:IPostObject
    {
        public LotteriesCompany CompanyId { get; set; }
        public int MaxDrawCount { get; set; }
        public List<LotteriesProduct> OptionalProductFilter { get; set; }
    }

    public enum LotteriesCompany
    {
        None,
        Tattersalls,
        GoldenCasket,
        NSWLotteries,
	    NTLotteries,
	    SALotteries
    };

    public enum LotteriesProduct
    {
        None,
        TattsLotto,
        OzLotto,
        Powerball,
        Super66,
        Pools,
        MonWedLotto,
        LuckyLotteries2,
        LuckyLotteries5,
        LottoStrike,
        WedLotto,
        Keno,
        CoinToss,
        SetForLife,
        MultiProduct,
        InstantScratchIts,
        TwoDollarCasket,
        BonusDraws
    };
}