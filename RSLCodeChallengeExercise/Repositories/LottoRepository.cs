﻿using BaseLibrary.Repositories.Base;
using RSLCodeChallengeExercise.Models;
using RSLCodeChallengeExercise.Repositories.Interfaces;

namespace RSLCodeChallengeExercise.Repositories
{
    public class LottoRepository : ILottoRepository
    {
        public OpenDrawsResponse GetOpenLotteriesDraw(OpenDrawsRequest openDrawsRequest)
        {
            if (openDrawsRequest == null) return null;
            LotteriesConnection _lottoConnection = new LotteriesConnection(Constants.Endpoints.OpenDraws);
            var requestObject = openDrawsRequest;
            var data = _lottoConnection.PostData<OpenDrawsResponse>(Constants.Endpoints.OpenDraws, requestObject);
            return data;
        }
    }
}