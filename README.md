# README #

Repo for RSL Code Challenge:  the sln created in VS 2019.

### What is this repository for? ###

* RSL Code Challenge
* Uses only one of the apis - https://data.api.thelott.com/sales/vmax/web/data/lotto/opendraws and dumps data to the view + unit test cases related to the repo method

### How do I get set up? ###

* Summary of set up: 
Download VS 2019 community edition and open the sln
* Configuration: 
NA
* Dependencies: 
Nuget packages 
* Database configuration: 
NA
* How to run tests: 
Open the only unit test project and run tests
* Deployment instructions: 
None since this runs from the sln

