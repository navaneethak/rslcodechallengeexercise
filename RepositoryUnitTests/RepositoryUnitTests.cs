﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RSLCodeChallengeExercise.Models;
using RSLCodeChallengeExercise.Repositories;
using RSLCodeChallengeExercise.Repositories.Interfaces;

namespace RepositoryUnitTests
{
    [TestClass]
    public class LottoRepoTest
    {
        [TestMethod]
        public void GetLotteriesDrawifRequestisNull()
        {
            LottoRepository lottoRepository = new LottoRepository();
            OpenDrawsResponse openDrawsResponse = lottoRepository.GetOpenLotteriesDraw(null);
            Assert.AreEqual(openDrawsResponse, null);
        }

        [TestMethod]
        public void GetLotteriesDrawifNoProductFilter()
        {
            ILottoRepository lottoRepository = new LottoRepository();
            OpenDrawsRequest openDrawsRequest = new OpenDrawsRequest
            {
                CompanyId = LotteriesCompany.Tattersalls,
                MaxDrawCount = 2
            };

            OpenDrawsResponse openDrawsResponse = lottoRepository.GetOpenLotteriesDraw(openDrawsRequest);
            Assert.AreEqual(openDrawsResponse.Draws.Count, 2);
        }

        [TestMethod]
        public void GetLotteriesDrawifCompanyIdisInValid()
        {
            ILottoRepository lottoRepository = new LottoRepository();
            OpenDrawsRequest openDrawsRequest = new OpenDrawsRequest
            {
                CompanyId = LotteriesCompany.None,
                MaxDrawCount = 2
            };
            List<LotteriesProduct> optionalProductFilter = new List<LotteriesProduct>
            {
                LotteriesProduct.Powerball,
                LotteriesProduct.Super66
            };
            openDrawsRequest.OptionalProductFilter = optionalProductFilter;
            OpenDrawsResponse openDrawsResponse = lottoRepository.GetOpenLotteriesDraw(openDrawsRequest);
            Assert.AreEqual(openDrawsResponse.Success, false);
        }

        [TestMethod]
        public void GetLotteriesDrawifProductFilterhasPowerball()
        {
            ILottoRepository lottoRepository = new LottoRepository();
            OpenDrawsRequest openDrawsRequest = new OpenDrawsRequest
            {
                CompanyId = LotteriesCompany.GoldenCasket,
                MaxDrawCount = 2
            };
            List<LotteriesProduct> optionalProductFilter = new List<LotteriesProduct>
            {
                LotteriesProduct.Powerball
            };
            openDrawsRequest.OptionalProductFilter = optionalProductFilter;
            OpenDrawsResponse openDrawsResponse = lottoRepository.GetOpenLotteriesDraw(openDrawsRequest);
            Assert.AreEqual(openDrawsResponse.Draws[0].ProductId.ToLower(), "powerball");
        }
    }
}
